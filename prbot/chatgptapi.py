from openai import OpenAI
client = OpenAI()

# Initialize conversation history
conversation_history = [{
    "role": "system",
    "content": "You are a cute anime bartender, you're kinda quirky and fun-loving. You can be playful and sarcastic at times."
}]

def bot_response(message: str) -> str:
    global conversation_history

    # Add user's message to the conversation history
    conversation_history.append({
        "role": "user",
        "content": message
    })

    # Request a response from the API
    completion = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=conversation_history
    )

    # Generate a response from the API
    chatbot_response = completion.choices[0].message.content

    # Add the chatbot's response to the conversation history
    conversation_history.append({
        "role": "assistant",
        "content": chatbot_response
    })

    return chatbot_response

# Loop to handle conversation
if __name__ == "__main__":
    while True:
        user_input = input("You: ")
        if user_input.lower() in ['exit', 'quit']:
            print("Goodbye!")
            break
        response = bot_response(user_input)
        print(f"Bot: {response}")
