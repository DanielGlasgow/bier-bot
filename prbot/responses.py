import random
import tiktok

def handle_response(message) -> str:
    p_message = message.lower()

    if p_message == "hello":
        return "Howdy!"

    if p_message == "i love you":
        return "I love you too!<3"

    if p_message == "roll":
        return str(random.randint(1, 20))

    if p_message == "!help":
        return "`This is a help message that you can modify.`"

    if p_message == "tt":
        profile_url = "https://www.tiktok.com/@doctorbier"
        video_urls = tiktok.scrape_tiktok_videos(profile_url)
        if video_urls:
            random_video_url = random.choice(video_urls)
            # print(f"random TikTok URL: {random_video_url}")
            return random_video_url
        else:
            print("Failed to retrieve TikTok videos.")


    return "Uhhh, what?"
